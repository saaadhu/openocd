/***************************************************************************
 *   Copyright (C) 2014 by Paul Fertser                                    *
 *   fercerpav@gmail.com                                                   *
 *                                                                         *
 *   Copyright (C) 2013 by mike brown                                      *
 *   mike@theshedworks.org.uk                                              *
 *                                                                         *
 *   Copyright (C) 2013 by Spencer Oliver                                  *
 *   spen@spen-soft.co.uk                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.           *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <transport/transport.h>
#include <jtag/swd.h>
#include <jtag/interface.h>
#include <jtag/commands.h>
#include <jtag/tcl.h>

#include <hidapi.h>

#include <stdio.h>
#include <stdlib.h>
#include <libwebsockets.h>
#include <pthread.h>


static void ws_send(char *b, unsigned len);
static char* ws_receive(unsigned int *len);

static void init_ws_server(void);

static int callback_http(struct lws *wsi,
                         enum lws_callback_reasons reason, void *user,
                         void *in, size_t len)
{
	return 0;
}

volatile unsigned int g_data_available;
volatile unsigned char  g_buf[512];
volatile size_t g_size;

volatile unsigned int g_data_read_available;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;


pthread_mutex_t recv_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t recv_cond = PTHREAD_COND_INITIALIZER;

static int callback_ws_cmsis_dap(struct lws *wsi,
                                   enum lws_callback_reasons reason,
                                   void *user, void *in, size_t len)
{
    switch (reason) {
        case LWS_CALLBACK_ESTABLISHED: // just log message that someone is connecting
            printf("connection established\n");
            lws_callback_on_writable(wsi);
            break;
        case LWS_CALLBACK_RECEIVE: { // the funny part
            /* printf ("Receiving\n"); */

			pthread_mutex_lock(&recv_mutex);
            memcpy ((void*)g_buf, in, len);
            g_size = len;
            g_data_read_available = 1;
			pthread_cond_signal(&recv_cond);
			pthread_mutex_unlock(&recv_mutex);

            
            break;
        }
    case LWS_CALLBACK_SERVER_WRITEABLE: {
        if (g_data_available == 0) {
            // schedule ourselves to run next tick anyway
            lws_callback_on_writable(wsi);
            return 0;
        }
        // send our asynchronous message
        lws_write(wsi, (unsigned char *)g_buf, g_size, LWS_WRITE_BINARY);
        pthread_mutex_lock(&mutex);
        g_data_available = 0;
        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex);
        // and schedule ourselves again
        lws_callback_on_writable(wsi);
        break;
    }
            

    default:
        break;
    }
    
    return 0;
}



static struct lws_protocols protocols[] = {
    /* first protocol must always be HTTP handler */
    {
        "http-only",   // name
        callback_http, // callback
        0,              // per_session_data_size
		1024,
		2, NULL
    },
    {
        "ws-cmsis-dap", // protocol name - very important!
        callback_ws_cmsis_dap,   // callback
        0,                          // we don't use any per session data
		1024,
		3, NULL
    },
    {
        NULL, NULL, 0, 0, 0, NULL   /* End of list */
    }
};

void* ws_start(void* p) {
    // server url will be http://localhost:9000
    int port = 9000;
    struct lws_context *context;

    struct lws_context_creation_info context_info =
    {
        .port = port, .iface = NULL, .protocols = protocols, .extensions = NULL,
        .ssl_cert_filepath = NULL, .ssl_private_key_filepath = NULL, .ssl_ca_filepath = NULL,
        .gid = -1, .uid = -1, .options = 0, NULL, .ka_time = 0, .ka_probes = 0, .ka_interval = 0
    }; 
    
    // create libwebsocket context representing this server
    context = lws_create_context(&context_info);
    
    if (context == NULL) {
        fprintf(stderr, "libwebsocket init failed\n");
        return NULL;
    }
    
    printf("starting server...\n");
    lws_callback_on_writable_all_protocol(context, protocols);
    
    // infinite loop, to end this server send SIGTERM. (CTRL+C)
    while (1) {
        lws_service(context, 50);
        // libwebsocket_service will process all waiting events with their
        // callback functions and then wait 50 ms.
        // (this is a single threaded webserver and this will keep our server
        // from generating load while there are not requests to process)
    }
    
    lws_context_destroy(context);
}

pthread_t ws_thread;
static void init_ws_server()
{
	int ret;
    if ((ret = pthread_create(&ws_thread, NULL, ws_start, NULL)))
    {
        fprintf(stderr, "pthread_create failed %d\n", ret);
    }
}

static void ws_send(char *b, unsigned len)
{
    memcpy((void*)g_buf, b, len);
    g_size = len;
    g_data_available = 1;

    pthread_mutex_lock(&mutex);
    while (g_data_available)
      pthread_cond_wait(&cond, &mutex);
    pthread_mutex_unlock(&mutex);
}


static char* ws_receive(unsigned int *len)
{
    pthread_mutex_lock(&recv_mutex);
    while (g_data_read_available == 0)
      pthread_cond_wait(&recv_cond, &recv_mutex);
    g_data_read_available = 0;
    *len = g_size;
    pthread_mutex_unlock(&recv_mutex);

    return (char*)g_buf;
}

/*
struct hid_device_info * ws_hid_enumerate (unsigned short vendor_id, unsigned short product_id)
{
    char buf[sizeof(hid_command) +
             (sizeof(unsigned short) * 2) + 1
             + LWS_SEND_BUFFER_PRE_PADDING + LWS_SEND_BUFFER_POST_PADDING]
        = {0};

    sprintf(buf, "%cu%hu%hu", ENUMERATE, vendor_id, product_id);
    ws_send (buf, sizeof(buf));

    unsigned int len = 0;
    char *retbuf = ws_receive (&len);

    // TODO: Error Handling
    struct hid_device_info *p = (struct hid_device_info *) malloc(sizeof(struct hid_device_info));
    memset (p, 0, sizeof(struct hid_device_info));
    
    sscanf (retbuf, "%s%hu%hu%s%hu%s%s%hu%hu%d", &p->path, &p->vendor_id, &p->product_id, &p->serial_number,
            &p->release_number, &p->manufacturer_string, &p->product_string,
            &p->usage_page, &p->usage, &p->interface_number);

    free (retbuf);

    return p;
}
*/

/*
 * See CMSIS-DAP documentation:
 * Version 0.01 - Beta.
 */

/* USB Config */

/* Known vid/pid pairs:
 * VID 0xc251: Keil Software
 * PID 0xf001: LPC-Link-II CMSIS_DAP
 * PID 0xf002: OPEN-SDA CMSIS_DAP (Freedom Board)
 * PID 0x2722: Keil ULINK2 CMSIS-DAP
 *
 * VID 0x0d28: mbed Software
 * PID 0x0204: MBED CMSIS-DAP
 */

#define MAX_USB_IDS 8
/* vid = pid = 0 marks the end of the list */
static uint16_t cmsis_dap_vid[MAX_USB_IDS + 1] = { 0 };
static uint16_t cmsis_dap_pid[MAX_USB_IDS + 1] = { 0 };
static wchar_t *cmsis_dap_serial;
static bool swd_mode;

#define PACKET_SIZE       (64 + 1)	/* 64 bytes plus report id */
#define USB_TIMEOUT       1000

/* CMSIS-DAP General Commands */
#define CMD_DAP_INFO              0x00
#define CMD_DAP_LED               0x01
#define CMD_DAP_CONNECT           0x02
#define CMD_DAP_DISCONNECT        0x03
#define CMD_DAP_WRITE_ABORT       0x08
#define CMD_DAP_DELAY             0x09
#define CMD_DAP_RESET_TARGET      0x0A

/* CMD_INFO */
#define INFO_ID_VID               0x00      /* string */
#define INFO_ID_PID               0x02      /* string */
#define INFO_ID_SERNUM            0x03      /* string */
#define INFO_ID_FW_VER            0x04      /* string */
#define INFO_ID_TD_VEND           0x05      /* string */
#define INFO_ID_TD_NAME           0x06      /* string */
#define INFO_ID_CAPS              0xf0      /* byte */
#define INFO_ID_PKT_CNT           0xfe      /* byte */
#define INFO_ID_PKT_SZ            0xff      /* short */

#define INFO_CAPS_SWD             0x01
#define INFO_CAPS_JTAG            0x02

/* CMD_LED */
#define LED_ID_CONNECT            0x00
#define LED_ID_RUN                0x01

#define LED_OFF                   0x00
#define LED_ON                    0x01

/* CMD_CONNECT */
#define CONNECT_DEFAULT           0x00
#define CONNECT_SWD               0x01
#define CONNECT_JTAG              0x02

/* CMSIS-DAP Common SWD/JTAG Commands */
#define CMD_DAP_DELAY             0x09
#define CMD_DAP_SWJ_PINS          0x10
#define CMD_DAP_SWJ_CLOCK         0x11
#define CMD_DAP_SWJ_SEQ           0x12

/*
 * PINS
 * Bit 0: SWCLK/TCK
 * Bit 1: SWDIO/TMS
 * Bit 2: TDI
 * Bit 3: TDO
 * Bit 5: nTRST
 * Bit 7: nRESET
 */

/* CMSIS-DAP SWD Commands */
#define CMD_DAP_SWD_CONFIGURE     0x13

/* CMSIS-DAP JTAG Commands */
#define CMD_DAP_JTAG_SEQ          0x14
#define CMD_DAP_JTAG_CONFIGURE    0x15
#define CMD_DAP_JTAG_IDCODE       0x16

/* CMSIS-DAP Transfer Commands */
#define CMD_DAP_TFER_CONFIGURE    0x04
#define CMD_DAP_TFER              0x05
#define CMD_DAP_TFER_BLOCK        0x06
#define CMD_DAP_TFER_ABORT        0x07

/* DAP Status Code */
#define DAP_OK                    0
#define DAP_ERROR                 0xFF

/* CMSIS-DAP Vendor Commands
 * None as yet... */

static const char * const info_caps_str[] = {
	"SWD  Supported",
	"JTAG Supported"
};

/* max clock speed (kHz) */
#define DAP_MAX_CLOCK             5000

struct cmsis_dap {
	hid_device *dev_handle;
	uint16_t packet_size;
	uint16_t packet_count;
	uint8_t *packet_buffer;
	uint8_t caps;
	uint8_t mode;
};

struct pending_transfer_result {
	uint8_t cmd;
	uint32_t data;
	void *buffer;
};

static int pending_transfer_count, pending_queue_len;
static struct pending_transfer_result *pending_transfers;

static int queued_retval;

static struct cmsis_dap *cmsis_dap_handle;
char cmdbuf[64];

static int cmsis_dap_usb_open(void)
{
    memset (cmdbuf, 0, sizeof(cmdbuf));
    strcpy(cmdbuf, "USB_Open");
    ws_send(cmdbuf, sizeof(cmdbuf));

	struct cmsis_dap *dap = malloc(sizeof(struct cmsis_dap));
	if (dap == NULL) {
		LOG_ERROR("unable to allocate memory");
		return ERROR_FAIL;
	}

	dap->dev_handle = NULL;
	dap->caps = 0;
	dap->mode = 0;

	cmsis_dap_handle = dap;

	/* allocate default packet buffer, may be changed later.
	 * currently with HIDAPI we have no way of getting the output report length
	 * without this info we cannot communicate with the adapter.
	 * For the moment we ahve to hard code the packet size */

	int packet_size = 512 + 1;

	cmsis_dap_handle->packet_buffer = malloc(packet_size);
	cmsis_dap_handle->packet_size = packet_size;

	if (cmsis_dap_handle->packet_buffer == NULL) {
		LOG_ERROR("unable to allocate memory");
		return ERROR_FAIL;
	}

	return ERROR_OK;
}

static void cmsis_dap_usb_close(struct cmsis_dap *dap)
{
    memset (cmdbuf, 0, sizeof(cmdbuf));
    strcpy(cmdbuf, "USB_Close");
    ws_send(cmdbuf, sizeof(cmdbuf));

	return;
}

static int cmsis_dap_cmd_DAP_SWJ_Pins(uint8_t pins, uint8_t mask, uint32_t delay, uint8_t *input)
{
    memset (cmdbuf, 0, sizeof(cmdbuf));
    strcpy(cmdbuf, "DAP_Swj_Pins");
    cmdbuf[14] = pins;
	cmdbuf[15] = mask;
	cmdbuf[16] = delay & 0xff;
	cmdbuf[17] = (delay >> 8) & 0xff;
	cmdbuf[18] = (delay >> 16) & 0xff;
	cmdbuf[19] = (delay >> 24) & 0xff;
    ws_send(cmdbuf, sizeof(cmdbuf));

	unsigned int len;
	uint8_t *recvbuf = (uint8_t *)ws_receive(&len);

    if (input)
        *input = *recvbuf;
	return ERROR_OK;
}

static int cmsis_dap_cmd_DAP_SWJ_Clock(uint32_t swj_clock)
{
	swj_clock *= 1000;
    memset (cmdbuf, 0, sizeof(cmdbuf));
    strcpy(cmdbuf, "DAP_Swj_Clock");
	cmdbuf[14] = swj_clock & 0xff;
	cmdbuf[15] = (swj_clock >> 8) & 0xff;
	cmdbuf[16] = (swj_clock >> 16) & 0xff;
	cmdbuf[17] = (swj_clock >> 24) & 0xff;
    ws_send(cmdbuf, sizeof(cmdbuf));

	unsigned int len;
	uint8_t *recvbuf = (uint8_t *)ws_receive(&len);
	if (*recvbuf != DAP_OK) {
		LOG_ERROR("CMSIS-DAP command CMD_DAP_SWJ_CLOCK failed.");
		return ERROR_JTAG_DEVICE_ERROR;
	}

	return ERROR_OK;
}

static int cmsis_dap_cmd_DAP_Info(uint8_t info, uint8_t **data)
{
    memset (cmdbuf, 0, sizeof(cmdbuf));
    strcpy(cmdbuf, "DAP_Info");
    cmdbuf[14] = info;
    ws_send(cmdbuf, sizeof(cmdbuf));

	unsigned int len;
	uint8_t *recvbuf = (uint8_t *)ws_receive(&len);

	*data = recvbuf;
	return ERROR_OK;
}

static int cmsis_dap_cmd_DAP_LED(uint8_t leds)
{
    memset (cmdbuf, 0, sizeof(cmdbuf));
    strcpy(cmdbuf, "DAP_Led");
	cmdbuf[14] = 0;
	cmdbuf[15] = leds;
    ws_send(cmdbuf, sizeof(cmdbuf));

	unsigned int len;
	uint8_t *recvbuf = (uint8_t *)ws_receive(&len);

	if (*recvbuf != DAP_OK) {
		LOG_ERROR("CMSIS-DAP command CMD_LED failed.");
		return ERROR_JTAG_DEVICE_ERROR;
	}


	return ERROR_OK;
}

static int cmsis_dap_cmd_DAP_Connect(uint8_t mode)
{
    memset (cmdbuf, 0, sizeof(cmdbuf));
    strcpy(cmdbuf, "DAP_Connect");
    cmdbuf[14] = mode;
    ws_send(cmdbuf, sizeof(cmdbuf));

	return ERROR_OK;
}

static int cmsis_dap_cmd_DAP_Disconnect(void)
{
    memset (cmdbuf, 0, sizeof(cmdbuf));
    strcpy(cmdbuf, "DAP_Disconnect");
    ws_send(cmdbuf, sizeof(cmdbuf));

	return ERROR_OK;
}

static int cmsis_dap_cmd_DAP_TFER_Configure(uint8_t idle, uint16_t retry_count, uint16_t match_retry)
{
    memset (cmdbuf, 0, sizeof(cmdbuf));
    strcpy(cmdbuf, "DAP_Tfer_Cfg");
	cmdbuf[14] = idle;
	cmdbuf[15] = retry_count & 0xff;
	cmdbuf[16] = (retry_count >> 8) & 0xff;
	cmdbuf[17] = match_retry & 0xff;
	cmdbuf[18] = (match_retry >> 8) & 0xff;
    ws_send(cmdbuf, sizeof(cmdbuf));

	unsigned int len;
	uint8_t *recvbuf = (uint8_t *)ws_receive(&len);

	if (*recvbuf != DAP_OK) {
		LOG_ERROR("CMSIS-DAP command CMD_TFER_Configure failed.");
		return ERROR_JTAG_DEVICE_ERROR;
	}

	return ERROR_OK;
}

static int cmsis_dap_cmd_DAP_SWD_Configure(uint8_t cfg)
{
    memset (cmdbuf, 0, sizeof(cmdbuf));
    strcpy(cmdbuf, "DAP_Swd_Cfg");
	cmdbuf[14] = cfg;
    ws_send(cmdbuf, sizeof(cmdbuf));

	unsigned int len;
	uint8_t *recvbuf = (uint8_t *)ws_receive(&len);

	if (*recvbuf != DAP_OK) {
		LOG_ERROR("CMSIS-DAP command CMD_SWD_Configure failed.");
		return ERROR_JTAG_DEVICE_ERROR;
	}

	return ERROR_OK;
}

#if 0
static int cmsis_dap_cmd_DAP_Delay(uint16_t delay_us)
{
	int retval;
	uint8_t *buffer = cmsis_dap_handle->packet_buffer;

	buffer[0] = 0;	/* report number */
	buffer[1] = CMD_DAP_DELAY;
	buffer[2] = delay_us & 0xff;
	buffer[3] = (delay_us >> 8) & 0xff;
	retval = cmsis_dap_usb_xfer(cmsis_dap_handle, 4);

	if (retval != ERROR_OK || buffer[1] != DAP_OK) {
		LOG_ERROR("CMSIS-DAP command CMD_Delay failed.");
		return ERROR_JTAG_DEVICE_ERROR;
	}

	return ERROR_OK;
}
#endif

static int cmsis_dap_swd_run_queue(void)
{
	LOG_DEBUG("Executing %d queued transactions", pending_transfer_count);

	if (queued_retval != ERROR_OK) {
		LOG_DEBUG("Skipping due to previous errors: %d", queued_retval);
		goto skip;
	}

	if (!pending_transfer_count)
		goto skip;

    uint32_t readbuffer[512] = {0};
    size_t readbuffer_index = 0;

	for (int i = 0; i < pending_transfer_count; i++) {
		uint8_t cmd = pending_transfers[i].cmd;
		uint32_t data = pending_transfers[i].data;

		LOG_DEBUG("%s %s reg %x %"PRIx32,
				  cmd & SWD_CMD_APnDP ? "AP" : "DP",
				  cmd & SWD_CMD_RnW ? "read" : "write",
				  (cmd & SWD_CMD_A32) >> 1, data);

		/* When proper WAIT handling is implemented in the
		 * common SWD framework, this kludge can be
		 * removed. However, this might lead to minor
		 * performance degradation as the adapter wouldn't be
		 * able to automatically retry anything (because ARM
		 * has forgotten to implement sticky error flags
		 * clearing). See also comments regarding
		 * cmsis_dap_cmd_DAP_TFER_Configure() and
		 * cmsis_dap_cmd_DAP_SWD_Configure() in
		 * cmsis_dap_init().
		 */
		if (!(cmd & SWD_CMD_RnW) &&
			!(cmd & SWD_CMD_APnDP) &&
			(cmd & SWD_CMD_A32) >> 1 == DP_CTRL_STAT &&
			(data & CORUNDETECT)) {
			LOG_DEBUG("refusing to enable sticky overrun detection");
			data &= ~CORUNDETECT;
		}


		memset (cmdbuf, 0, sizeof(cmdbuf));
		strcpy(cmdbuf, "DAP_Tfer");
		cmdbuf[14] = 1;
		cmdbuf[15] = (cmd >> 1) & 0x0f;

		if (!(cmd & SWD_CMD_RnW)) {
			cmdbuf[16] = (data) & 0xff;
			cmdbuf[17] = (data >> 8) & 0xff;
			cmdbuf[18] = (data >> 16) & 0xff;
			cmdbuf[19] = (data >> 24) & 0xff;
		}
		ws_send(cmdbuf, sizeof(cmdbuf));
        queued_retval = ERROR_OK;

		if (pending_transfers[i].cmd & SWD_CMD_RnW) {
            unsigned int len;
            const uint8_t *recvbuf = (const uint8_t *)ws_receive(&len);
			uint32_t dat = le_to_h_u32(recvbuf);
            readbuffer[readbuffer_index++] = dat;
		}
	}

    size_t read_index = 0;
    for (int i = 0; i < pending_transfer_count; i++) {
        if (pending_transfers[i].cmd & SWD_CMD_RnW) {
            static uint32_t last_read;
            uint32_t data = readbuffer[read_index++];
            uint32_t tmp = data;
  
            LOG_DEBUG("Read result: %"PRIx32, data);
  
            /* Imitate posted AP reads */
            if ((pending_transfers[i].cmd & SWD_CMD_APnDP) ||
                ((pending_transfers[i].cmd & SWD_CMD_A32) >> 1 == DP_RDBUFF)) {
                tmp = last_read;
                last_read = data;
            }
  
            if (pending_transfers[i].buffer)
                *(uint32_t *)pending_transfers[i].buffer = tmp;
        }
    }

skip:
    pending_transfer_count = 0;
    int retval = queued_retval;
    queued_retval = ERROR_OK;

    return retval;
}

static void cmsis_dap_swd_queue_cmd(uint8_t cmd, uint32_t *dst, uint32_t data)
{
	if (pending_transfer_count == pending_queue_len) {
		/* Not enough room in the queue. Run the queue. */
		queued_retval = cmsis_dap_swd_run_queue();
	}

	if (queued_retval != ERROR_OK)
		return;

	pending_transfers[pending_transfer_count].data = data;
	pending_transfers[pending_transfer_count].cmd = cmd;
	if (cmd & SWD_CMD_RnW) {
		/* Queue a read transaction */
		pending_transfers[pending_transfer_count].buffer = dst;
	}
	pending_transfer_count++;
}

static void cmsis_dap_swd_write_reg(uint8_t cmd, uint32_t value, uint32_t ap_delay_clk)
{
	assert(!(cmd & SWD_CMD_RnW));
	cmsis_dap_swd_queue_cmd(cmd, NULL, value);
}

static void cmsis_dap_swd_read_reg(uint8_t cmd, uint32_t *value, uint32_t ap_delay_clk)
{
	assert(cmd & SWD_CMD_RnW);
	cmsis_dap_swd_queue_cmd(cmd, value, 0);
}

static int cmsis_dap_get_version_info(void)
{
	uint8_t *data;

	/* INFO_ID_FW_VER - string */
	int retval = cmsis_dap_cmd_DAP_Info(INFO_ID_FW_VER, &data);
	if (retval != ERROR_OK)
		return retval;

	if (data[0]) /* strlen */
		LOG_INFO("CMSIS-DAP: FW Version = %s", &data[1]);

	return ERROR_OK;
}

static int cmsis_dap_get_caps_info(void)
{
	uint8_t *data;

	/* INFO_ID_CAPS - byte */
	int retval = cmsis_dap_cmd_DAP_Info(INFO_ID_CAPS, &data);
	if (retval != ERROR_OK)
		return retval;

	if (data[0] == 1) {
		uint8_t caps = data[1];

		cmsis_dap_handle->caps = caps;

		if (caps & INFO_CAPS_SWD)
			LOG_INFO("CMSIS-DAP: %s", info_caps_str[0]);
		if (caps & INFO_CAPS_JTAG)
			LOG_INFO("CMSIS-DAP: %s", info_caps_str[1]);
	}

	return ERROR_OK;
}

static int cmsis_dap_get_status(void)
{
	uint8_t d;

	int retval = cmsis_dap_cmd_DAP_SWJ_Pins(0, 0, 0, &d);

	if (retval == ERROR_OK) {
		LOG_INFO("SWCLK/TCK = %d SWDIO/TMS = %d TDI = %d TDO = %d nTRST = %d nRESET = %d",
			(d & (0x01 << 0)) ? 1 : 0,	/* Bit 0: SWCLK/TCK */
			(d & (0x01 << 1)) ? 1 : 0,	/* Bit 1: SWDIO/TMS */
			(d & (0x01 << 2)) ? 1 : 0,	/* Bit 2: TDI */
			(d & (0x01 << 3)) ? 1 : 0,	/* Bit 3: TDO */
			(d & (0x01 << 5)) ? 1 : 0,	/* Bit 5: nTRST */
			(d & (0x01 << 7)) ? 1 : 0);	/* Bit 7: nRESET */
	}

	return retval;
}

static int cmsis_dap_swd_switch_seq(enum swd_special_seq seq)
{
	const uint8_t *s;
	unsigned int s_len;
	int retval;

	/* First disconnect before connecting, Atmel EDBG needs it for SAMD/R/L/C */
	cmsis_dap_cmd_DAP_Disconnect();

	/* When we are reconnecting, DAP_Connect needs to be rerun, at
	 * least on Keil ULINK-ME */
	retval = cmsis_dap_cmd_DAP_Connect(seq == LINE_RESET || seq == JTAG_TO_SWD ?
					   CONNECT_SWD : CONNECT_JTAG);
	if (retval != ERROR_OK)
		return retval;

	switch (seq) {
	case LINE_RESET:
		LOG_DEBUG("SWD line reset");
		s = swd_seq_line_reset;
		s_len = swd_seq_line_reset_len;
		break;
	case JTAG_TO_SWD:
		LOG_DEBUG("JTAG-to-SWD");
		s = swd_seq_jtag_to_swd;
		s_len = swd_seq_jtag_to_swd_len;
		break;
	case SWD_TO_JTAG:
		LOG_DEBUG("SWD-to-JTAG");
		s = swd_seq_swd_to_jtag;
		s_len = swd_seq_swd_to_jtag_len;
		break;
	default:
		LOG_ERROR("Sequence %d not supported", seq);
		return ERROR_FAIL;
	}

    memset (cmdbuf, 0, sizeof(cmdbuf));
    strcpy(cmdbuf, "DAP_Swj_Seq");
    cmdbuf[14] = DIV_ROUND_UP(s_len, 8) + 3;
	bit_copy((uint8_t*)&cmdbuf[15], 0, s, 0, s_len);
    ws_send(cmdbuf, sizeof(cmdbuf));

	unsigned int len;
	uint8_t *recvbuf = (uint8_t *)ws_receive(&len);

	if (*recvbuf != DAP_OK) {
		return ERROR_FAIL;
	}

	return ERROR_OK;
}

static int cmsis_dap_swd_open(void)
{
	int retval;

	if (cmsis_dap_handle == NULL) {
		/* SWD init */
		retval = cmsis_dap_usb_open();
		if (retval != ERROR_OK)
			return retval;
	}

	pending_queue_len = (512 - 4) / 5;
	pending_transfers = malloc(pending_queue_len * sizeof(*pending_transfers));
	if (!pending_transfers) {
		LOG_ERROR("Unable to allocate memory for CMSIS-DAP queue");
		return ERROR_FAIL;
	}

	retval = cmsis_dap_cmd_DAP_Connect(CONNECT_SWD);
	if (retval != ERROR_OK)
		return retval;

	/* Add more setup here.??... */

	LOG_INFO("CMSIS-DAP: Interface Initialised (SWD)");
	return ERROR_OK;
}

static int cmsis_dap_init(void)
{
	int retval;
	uint8_t *data;

	init_ws_server();

	if (swd_mode) {
		retval = cmsis_dap_swd_open();
		if (retval != ERROR_OK)
			return retval;
	}

	if (cmsis_dap_handle == NULL) {

		/* JTAG init */
		retval = cmsis_dap_usb_open();
		if (retval != ERROR_OK)
			return retval;

		retval = cmsis_dap_get_caps_info();
		if (retval != ERROR_OK)
			return retval;

		/* Connect in JTAG mode */
		if (!(cmsis_dap_handle->caps & INFO_CAPS_JTAG)) {
			LOG_ERROR("CMSIS-DAP: JTAG not supported");
			return ERROR_JTAG_DEVICE_ERROR;
		}

		retval = cmsis_dap_cmd_DAP_Connect(CONNECT_JTAG);
		if (retval != ERROR_OK)
			return retval;

		LOG_INFO("CMSIS-DAP: Interface Initialised (JTAG)");
	}

	retval = cmsis_dap_get_version_info();
	if (retval != ERROR_OK)
		return retval;

	/* INFO_ID_PKT_SZ - short */
	retval = cmsis_dap_cmd_DAP_Info(INFO_ID_PKT_SZ, &data);
	if (retval != ERROR_OK)
		return retval;

	if (data[0] == 2) {  /* short */
		uint16_t pkt_sz = data[1] + (data[2] << 8);

		/* 4 bytes of command header + 5 bytes per register
		 * write. For bulk read sequences just 4 bytes are
		 * needed per transfer, so this is suboptimal. */
		pending_queue_len = (pkt_sz - 4) / 5;
		pending_transfers = malloc(pending_queue_len * sizeof(*pending_transfers));
		if (!pending_transfers) {
			LOG_ERROR("Unable to allocate memory for CMSIS-DAP queue");
			return ERROR_FAIL;
		}

		if (cmsis_dap_handle->packet_size != pkt_sz + 1) {
			/* reallocate buffer */
			cmsis_dap_handle->packet_size = pkt_sz + 1;
			cmsis_dap_handle->packet_buffer = realloc(cmsis_dap_handle->packet_buffer,
					cmsis_dap_handle->packet_size);
			if (cmsis_dap_handle->packet_buffer == NULL) {
				LOG_ERROR("unable to reallocate memory");
				return ERROR_FAIL;
			}
		}

		LOG_DEBUG("CMSIS-DAP: Packet Size = %" PRId16, pkt_sz);
	}

	/* INFO_ID_PKT_CNT - byte */
	retval = cmsis_dap_cmd_DAP_Info(INFO_ID_PKT_CNT, &data);
	if (retval != ERROR_OK)
		return retval;

	if (data[0] == 1) { /* byte */
		uint16_t pkt_cnt = data[1];
		cmsis_dap_handle->packet_count = pkt_cnt;
		LOG_DEBUG("CMSIS-DAP: Packet Count = %" PRId16, pkt_cnt);
	}

	retval = cmsis_dap_get_status();
	if (retval != ERROR_OK)
		return ERROR_FAIL;

	/* Now try to connect to the target
	 * TODO: This is all SWD only @ present */
	retval = cmsis_dap_cmd_DAP_SWJ_Clock(jtag_get_speed_khz());
	if (retval != ERROR_OK)
		return ERROR_FAIL;

	/* Ask CMSIS-DAP to automatically retry on receiving WAIT for
	 * up to 64 times. This must be changed to 0 if sticky
	 * overrun detection is enabled. */
	retval = cmsis_dap_cmd_DAP_TFER_Configure(0, 64, 0);
	if (retval != ERROR_OK)
		return ERROR_FAIL;
	/* Data Phase (bit 2) must be set to 1 if sticky overrun
	 * detection is enabled */
	retval = cmsis_dap_cmd_DAP_SWD_Configure(0);	/* 1 TRN, no Data Phase */
	if (retval != ERROR_OK)
		return ERROR_FAIL;

	retval = cmsis_dap_cmd_DAP_LED(0x03);		/* Both LEDs on */
	if (retval != ERROR_OK)
		return ERROR_FAIL;

	/* support connecting with srst asserted */
	enum reset_types jtag_reset_config = jtag_get_reset_config();

	if (jtag_reset_config & RESET_CNCT_UNDER_SRST) {
		if (jtag_reset_config & RESET_SRST_NO_GATING) {
			retval = cmsis_dap_cmd_DAP_SWJ_Pins(0, (1 << 7), 0, NULL);
			if (retval != ERROR_OK)
				return ERROR_FAIL;
			LOG_INFO("Connecting under reset");
		}
	}

	cmsis_dap_cmd_DAP_LED(0x00);			/* Both LEDs off */

	LOG_INFO("CMSIS-DAP: Interface ready");

	return ERROR_OK;
}

static int cmsis_dap_swd_init(void)
{
	swd_mode = true;
	return ERROR_OK;
}

static int cmsis_dap_quit(void)
{
	cmsis_dap_cmd_DAP_Disconnect();
	cmsis_dap_cmd_DAP_LED(0x00);		/* Both LEDs off */

	cmsis_dap_usb_close(cmsis_dap_handle);

	return ERROR_OK;
}

static void cmsis_dap_execute_reset(struct jtag_command *cmd)
{
	int retval = cmsis_dap_cmd_DAP_SWJ_Pins(cmd->cmd.reset->srst ? 0 : (1 << 7), \
			(1 << 7), 0, NULL);
	if (retval != ERROR_OK)
		LOG_ERROR("CMSIS-DAP: Interface reset failed");
}

static void cmsis_dap_execute_sleep(struct jtag_command *cmd)
{
#if 0
	int retval = cmsis_dap_cmd_DAP_Delay(cmd->cmd.sleep->us);
	if (retval != ERROR_OK)
#endif
		jtag_sleep(cmd->cmd.sleep->us);
}

static void cmsis_dap_execute_command(struct jtag_command *cmd)
{
	switch (cmd->type) {
		case JTAG_RESET:
			cmsis_dap_execute_reset(cmd);
			break;
		case JTAG_SLEEP:
			cmsis_dap_execute_sleep(cmd);
			break;
		default:
			LOG_ERROR("BUG: unknown JTAG command type encountered");
			exit(-1);
	}
}

static int cmsis_dap_execute_queue(void)
{
	struct jtag_command *cmd = jtag_command_queue;

	while (cmd != NULL) {
		cmsis_dap_execute_command(cmd);
		cmd = cmd->next;
	}

	return ERROR_OK;
}

static int cmsis_dap_speed(int speed)
{
	if (speed > DAP_MAX_CLOCK) {
		LOG_INFO("reduce speed request: %dkHz to %dkHz maximum", speed, DAP_MAX_CLOCK);
		speed = DAP_MAX_CLOCK;
	}

	if (speed == 0) {
		LOG_INFO("RTCK not supported");
		return ERROR_JTAG_NOT_IMPLEMENTED;
	}

	return cmsis_dap_cmd_DAP_SWJ_Clock(speed);
}

static int cmsis_dap_speed_div(int speed, int *khz)
{
	*khz = speed;
	return ERROR_OK;
}

static int cmsis_dap_khz(int khz, int *jtag_speed)
{
	*jtag_speed = khz;
	return ERROR_OK;
}

static int_least32_t cmsis_dap_swd_frequency(int_least32_t hz)
{
	if (hz > 0)
		cmsis_dap_speed(hz / 1000);

	return hz;
}

COMMAND_HANDLER(cmsis_dap_handle_info_command)
{
	if (cmsis_dap_get_version_info() == ERROR_OK)
		cmsis_dap_get_status();

	return ERROR_OK;
}

COMMAND_HANDLER(cmsis_dap_handle_vid_pid_command)
{
	if (CMD_ARGC > MAX_USB_IDS * 2) {
		LOG_WARNING("ignoring extra IDs in cmsis_dap_vid_pid "
			"(maximum is %d pairs)", MAX_USB_IDS);
		CMD_ARGC = MAX_USB_IDS * 2;
	}
	if (CMD_ARGC < 2 || (CMD_ARGC & 1)) {
		LOG_WARNING("incomplete cmsis_dap_vid_pid configuration directive");
		if (CMD_ARGC < 2)
			return ERROR_COMMAND_SYNTAX_ERROR;
		/* remove the incomplete trailing id */
		CMD_ARGC -= 1;
	}

	unsigned i;
	for (i = 0; i < CMD_ARGC; i += 2) {
		COMMAND_PARSE_NUMBER(u16, CMD_ARGV[i], cmsis_dap_vid[i >> 1]);
		COMMAND_PARSE_NUMBER(u16, CMD_ARGV[i + 1], cmsis_dap_pid[i >> 1]);
	}

	/*
	 * Explicitly terminate, in case there are multiples instances of
	 * cmsis_dap_vid_pid.
	 */
	cmsis_dap_vid[i >> 1] = cmsis_dap_pid[i >> 1] = 0;

	return ERROR_OK;
}

COMMAND_HANDLER(cmsis_dap_handle_serial_command)
{
	if (CMD_ARGC == 1) {
		size_t len = mbstowcs(NULL, CMD_ARGV[0], 0);
		cmsis_dap_serial = calloc(len + 1, sizeof(wchar_t));
		if (cmsis_dap_serial == NULL) {
			LOG_ERROR("unable to allocate memory");
			return ERROR_OK;
		}
		if (mbstowcs(cmsis_dap_serial, CMD_ARGV[0], len + 1) == (size_t)-1) {
			free(cmsis_dap_serial);
			cmsis_dap_serial = NULL;
			LOG_ERROR("unable to convert serial");
		}
	} else {
		LOG_ERROR("expected exactly one argument to cmsis_dap_serial <serial-number>");
	}

	return ERROR_OK;
}

static const struct command_registration cmsis_dap_subcommand_handlers[] = {
	{
		.name = "info",
		.handler = &cmsis_dap_handle_info_command,
		.mode = COMMAND_EXEC,
		.usage = "",
		.help = "show cmsis-dap info",
	},
	COMMAND_REGISTRATION_DONE
};

static const struct command_registration cmsis_dap_command_handlers[] = {
	{
		.name = "cmsis-dap",
		.mode = COMMAND_ANY,
		.help = "perform CMSIS-DAP management",
		.usage = "<cmd>",
		.chain = cmsis_dap_subcommand_handlers,
	},
	{
		.name = "cmsis_dap_vid_pid",
		.handler = &cmsis_dap_handle_vid_pid_command,
		.mode = COMMAND_CONFIG,
		.help = "the vendor ID and product ID of the CMSIS-DAP device",
		.usage = "(vid pid)* ",
	},
	{
		.name = "cmsis_dap_serial",
		.handler = &cmsis_dap_handle_serial_command,
		.mode = COMMAND_CONFIG,
		.help = "set the serial number of the adapter",
		.usage = "serial_string",
	},
	COMMAND_REGISTRATION_DONE
};

static const struct swd_driver cmsis_dap_swd_driver = {
	.init = cmsis_dap_swd_init,
	.frequency = cmsis_dap_swd_frequency,
	.switch_seq = cmsis_dap_swd_switch_seq,
	.read_reg = cmsis_dap_swd_read_reg,
	.write_reg = cmsis_dap_swd_write_reg,
	.run = cmsis_dap_swd_run_queue,
};

static const char * const cmsis_dap_transport[] = { "swd", NULL };

struct jtag_interface cmsis_dap_interface = {
	.name = "cmsis-dap",
	.commands = cmsis_dap_command_handlers,
	.swd = &cmsis_dap_swd_driver,
	.transports = cmsis_dap_transport,

	.execute_queue = cmsis_dap_execute_queue,
	.speed = cmsis_dap_speed,
	.speed_div = cmsis_dap_speed_div,
	.khz = cmsis_dap_khz,
	.init = cmsis_dap_init,
	.quit = cmsis_dap_quit,
};
